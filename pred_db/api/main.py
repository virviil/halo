from clickhouse_driver import Client
from datetime import datetime

from fastapi import FastAPI, HTTPException
from .services.predictions import PredictionsService

from pydantic import BaseModel

from ..config import config


app = FastAPI(
    title="Halo Predictions",
    description="Predictions for devices",
    version="0.1.0",
)
client = Client(config.clickhouse_host)


class Probe(BaseModel):
    min: float
    max: float
    average: float

    @staticmethod
    def from_tuple(data_tuple):
        min, avg, max = data_tuple
        return Probe(min=min, average=avg, max=max)


class Prediction(BaseModel):
    device_id: int
    date: str
    drop_rate: Probe
    latency: Probe
    throughput: Probe

    @staticmethod
    def from_prediction_results(device_id, date, predictions_data):
        (
            (drop_rate_prediction, _),
            (latency_prediction, _),
            (throughput_prediction, _),
        ) = predictions_data

        return Prediction(
            device_id=device_id,
            date=str(date),
            latency=Probe.from_tuple(latency_prediction),
            drop_rate=Probe.from_tuple(drop_rate_prediction),
            throughput=Probe.from_tuple(throughput_prediction),
        )


@app.get("/devices/{device_id}/predict", response_model=Prediction)
def get_prediction(device_id: int):
    current_date = datetime.utcnow().date()
    predictions_data = PredictionsService(client).predict_for_date(
        device_id, current_date
    )
    if not predictions_data:
        raise HTTPException(status_code=404)
    return Prediction.from_prediction_results(device_id, current_date, predictions_data)
