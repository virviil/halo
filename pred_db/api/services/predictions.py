from clickhouse_driver import Client
from datetime import date

from pred_db.predictor.linear import LinearPredictor
from pred_db.db.contexts.predictions import PredictionsContext


class PredictionsService:
    def __init__(self, ch_client: Client) -> None:
        self._client = ch_client

    def predict_for_date(self, device_id: int, prediction_date: date):
        context = PredictionsContext(self._client)
        predictions_data = context.get_predictions(device_id, prediction_date)

        drop_rate_list, latency_list, throughput_list = [], [], []
        for drop_rate, latency, throughput in predictions_data:
            drop_rate_list.append(drop_rate)
            latency_list.append(latency)
            throughput_list.append(throughput)

        if (
            len(drop_rate_list) == 0
            or len(latency_list) == 0
            or len(throughput_list) == 0
        ):
            return None

        predictor = LinearPredictor()

        calculated_drop_rate = predictor.predict(drop_rate_list)
        calculated_latency = predictor.predict(latency_list)
        calculated_throughput = predictor.predict(throughput_list)

        return (
            (calculated_drop_rate, drop_rate_list),
            (calculated_latency, latency_list),
            (calculated_throughput, throughput_list),
        )
