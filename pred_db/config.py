from dataclasses import dataclass
from os import getenv


@dataclass
class Config:
    clickhouse_host: str = getenv("CLICKHOUSE_HOST") or "localhost"
    rmq_host: str = getenv("RMQ_HOST") or "localhost"


config = Config()
