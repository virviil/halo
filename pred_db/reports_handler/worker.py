from typing import List, Tuple
from celery import Celery
from datetime import datetime

from statistics import mean

from pred_db.db.contexts.reports import ReportsContext
from pred_db.db.contexts.predictions import PredictionsContext

from clickhouse_driver import Client

from ..config import config

app = Celery("reports_handler", broker=f"amqp://guest@{config.rmq_host}//")


@app.task(acks_late=True)
def calculate_probes(device_id: int, date: str):
    date = datetime.strptime(date, "%Y-%m-%d").date()
    ch_client = Client(config.clickhouse_host)

    reports_data = ReportsContext(ch_client).get_reports(device_id, date)

    # Skipping if no data is given - for simplicity
    if reports_data == []:
        return

    probe_data = get_probe_from_data(reports_data)

    return PredictionsContext(ch_client).insert_predictions_data(
        [[device_id, date, *list(probe_data)]]
    )


def get_probe_from_data(
    reports_data: List[Tuple[int, int, int]]
) -> Tuple[
    Tuple[float, float, float], Tuple[float, float, float], Tuple[float, float, float]
]:
    drop_rate_list = []
    latency_list = []
    throughput_list = []

    for drop_rate, latency, throughput in reports_data:
        drop_rate_list.append(drop_rate)
        latency_list.append(latency)
        throughput_list.append(throughput)

    return (
        (min(drop_rate_list), mean(drop_rate_list), max(drop_rate_list)),
        (min(latency_list), mean(latency_list), max(latency_list)),
        (min(throughput_list), mean(throughput_list), max(throughput_list)),
    )
