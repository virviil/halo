import datetime
import logging
from clickhouse_driver import Client

from pytz import UTC

from ..config import config

from .worker import calculate_probes

from apscheduler.schedulers.background import BlockingScheduler


def handle_reports():
    logging.info("Scheduling calculation tasks")

    # No need to recalculate date for 10000 times
    yesterday = datetime.datetime.utcnow().date() - datetime.timedelta(days=-1)

    for device_id in range(1, 10001):
        calculate_probes.delay(device_id, str(yesterday))


if __name__ == "__main__":
    logging.basicConfig(
        format="%(asctime)s %(message)s",
        datefmt="%d/%m/%Y %H:%M:%S",
        level=logging.INFO,
    )

    logging.info("Starting reports handler")

    scheduler = BlockingScheduler()

    ch_client = Client(config.clickhouse_host)

    scheduler.add_job(
        handle_reports,
        trigger="cron",
        name="reports_handler",
        id="reports_handler",
        hour="1",
        timezone=UTC,
    )
    scheduler.start()
