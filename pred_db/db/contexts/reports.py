from typing import List
from clickhouse_driver import Client
from datetime import date


class ReportsContext:
    def __init__(self, ch_client: Client) -> None:
        self._client = ch_client

    def get_reports(self, device_id: int, given_date: date):
        return self._client.execute(
            """
            SELECT
                drop_rate, latency, throughput
            FROM
                halo.reports
            WHERE
                toDate(datetime) = %(date)s and device_id = %(device_id)s
            ORDER BY
                timestamp DESC
            """,
            {"device_id": device_id, "date": given_date},
        )

    def insert_reports_data(self, reports_data: List[List[any]]):
        return self._client.execute(
            "INSERT INTO halo.reports (device_id, timestamp, drop_rate, latency, throughput) VALUES",
            reports_data,
        )
