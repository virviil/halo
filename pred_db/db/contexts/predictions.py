from typing import List
from clickhouse_driver import Client
from datetime import date


class PredictionsContext:
    def __init__(self, ch_client: Client) -> None:
        self._client = ch_client

    def get_predictions(self, device_id: int, prediction_date: date):
        return self._client.execute(
            """
            SELECT
                drop_rate, latency, throughput
            FROM
                halo.predictions
            WHERE
                device_id = %(device_id)s AND day_of_week = toDayOfWeek(toDate(%(date)s))
            ORDER BY
                date DESC
            LIMIT 10
            """,
            {"device_id": device_id, "date": prediction_date},
        )

    def insert_predictions_data(self, predictions_data: List[List[any]]):
        return self._client.execute(
            "INSERT INTO halo.predictions (device_id, date, drop_rate, latency, throughput) VALUES",
            predictions_data,
        )
