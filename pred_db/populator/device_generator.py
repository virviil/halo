import csv
from faker import Faker
from faker.providers import geo
from pathlib import Path


def generate_devices(number_of_devices: int, data_file_path: Path):
    faker = Faker()
    faker.add_provider(geo)

    with open(data_file_path, "w") as csv_writer:
        writer = csv.writer(csv_writer)
        for _device in range(0, number_of_devices):
            (lat, lng) = faker.latlng()
            writer.writerow([lat, lng])


if __name__ == "__main__":
    generate_devices(10000, "data/devices.csv")
