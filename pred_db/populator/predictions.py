from datetime import datetime, timedelta
import logging
from random import randrange

from more_itertools import chunked
from clickhouse_driver import Client

from pred_db.db.contexts.predictions import PredictionsContext


def backward_dates_generator(number_of_dates_to_generate):
    current_utc_date = datetime.utcnow().date()
    for days_offset in range(0, number_of_dates_to_generate):
        yield current_utc_date - timedelta(days=days_offset)


def write_fake_data_to_db(ch_client: Client):
    logging.info("Writing fake data to db")

    for date in backward_dates_generator(90):
        logging.info(f"Writing data for date: {date}")
        for device_id_chunk in chunked(range(1, 10001), 1000):
            PredictionsContext(ch_client).insert_predictions_data(
                [
                    # Here list comprehension builds list og 1000 values to make a bulk write
                    [
                        device_id,
                        date,
                        (randrange(3), randrange(3, 7), randrange(7, 10)),
                        (randrange(10, 30), randrange(30, 70), randrange(70, 100)),
                        (
                            randrange(100, 300),
                            randrange(300, 700),
                            randrange(700, 1000),
                        ),
                    ]
                    for device_id in device_id_chunk
                ]
            )


if __name__ == "__main__":
    logging.basicConfig(
        format="%(asctime)s %(message)s",
        datefmt="%d/%m/%Y %H:%M:%S",
        level=logging.INFO,
    )

    ch_client = Client("localhost")
    write_fake_data_to_db(ch_client)
