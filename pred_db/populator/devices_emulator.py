import time
import logging
from random import randrange

from more_itertools import chunked
from clickhouse_driver import Client

from ..config import config

from apscheduler.schedulers.background import BlockingScheduler


def write_fake_data_to_db(ch_client: Client):
    logging.info("Writing fake data to db")

    # No need to recalculate time for 10000 times - for emulations it's enogh if it will be the same
    current_time = int(time.time())

    for device_id_chunk in chunked(range(1, 10001), 1000):
        ch_client.execute(
            "INSERT INTO halo.reports (device_id, timestamp, drop_rate, latency, throughput) VALUES",
            [
                # Here list comprehension builds list og 1000 values to make a bulk write
                [
                    device_id,
                    current_time,
                    randrange(1000),
                    randrange(1000),
                    randrange(1000),
                ]
                for device_id in device_id_chunk
            ],
        )


if __name__ == "__main__":
    logging.basicConfig(
        format="%(asctime)s %(message)s",
        datefmt="%d/%m/%Y %H:%M:%S",
        level=logging.INFO,
    )

    logging.info("Starting device emulator")

    scheduler = BlockingScheduler()

    ch_client = Client(config.clickhouse_host)

    scheduler.add_job(
        lambda: write_fake_data_to_db(ch_client),
        trigger="cron",
        name="emulate_devices",
        id="emulate_devices",
        second="*/10",
    )
    scheduler.start()
