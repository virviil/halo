import time
import logging
from random import randrange

from more_itertools import chunked
from clickhouse_driver import Client

from pred_db.db.contexts.reports import ReportsContext


def backward_datetime_generator(number_of_datetimes_to_generate):
    current_unix_time = int(time.time())
    for hours_offset in range(0, number_of_datetimes_to_generate):
        yield current_unix_time - 60 * 60 * hours_offset


def write_fake_data_to_db(ch_client: Client):
    logging.info("Writing fake data to db")

    for datetime in backward_datetime_generator(90):
        logging.info(f"Writing data for date: {datetime}")
        for device_id_chunk in chunked(range(1, 10001), 1000):
            ReportsContext(ch_client).insert_reports_data(
                [
                    # Here list comprehension builds list og 1000 values to make a bulk write
                    [
                        device_id,
                        datetime,
                        randrange(10),
                        randrange(10, 100),
                        randrange(100, 1000),
                    ]
                    for device_id in device_id_chunk
                ]
            )


if __name__ == "__main__":
    logging.basicConfig(
        format="%(asctime)s %(message)s",
        datefmt="%d/%m/%Y %H:%M:%S",
        level=logging.INFO,
    )

    ch_client = Client("localhost")
    write_fake_data_to_db(ch_client)
