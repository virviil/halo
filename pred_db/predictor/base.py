import abc
from typing import List


class BasePredictor(metaclass=abc.ABCMeta):
    @abc.abstractmethod
    def predict(historical_data: List[float]) -> float:
        raise NotImplementedError
