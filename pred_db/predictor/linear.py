from typing import List, Tuple
from .base import BasePredictor

import operator


class LinearPredictor(BasePredictor):
    def predict(self, historical_data: List[Tuple[float, float, float]]) -> float:
        max_weight = len(historical_data)
        dividend, divisor = (0, 0, 0), 0

        for i in range(max_weight):
            dividend = tuple(
                map(
                    operator.add,
                    dividend,
                    tuple([(max_weight - i) * val for val in historical_data[i]]),
                )
            )
            divisor += max_weight - i

        return tuple([val / divisor for val in dividend])
