# Halo

## Running the swarm

Use `docker-compose up` to run the bunch of microservices. This command will start:

* `clockhouse` - main database service
* `rabbitmq` - broker for parallel tasks
* `emulator` - service that is emulating 10000 devices that are writing there data into reports db
* `scheduler` - main orchestartor for per-day reports to predictions calculations
* `worker` - 3 instances of workers that are doing the job
* `api` - web-api that can be used to reach the predictions data


### Scheduler

Will start the calculations every day at 1:00 UTC for the previous day reports data for each device

### API

Can be got from swagger, located at `localhost:8000/redoc` (if running from `compose`)

## DB structure

Can be found at `docker/clickhouse/docker-entrypoint-initdb.d/init-db.sh`

## Populating reports database

In normal flow, we need to wait at least 7 days to get some prediction results.
Still, random data can be populated for previoud 90 days with the next command:

```
python -m pred_db.populator.reports
```
