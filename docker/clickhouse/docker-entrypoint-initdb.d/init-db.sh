#!/bin/bash
set -e

clickhouse client -n <<-EOSQL
    CREATE DATABASE IF NOT EXISTS halo;
    CREATE TABLE IF NOT EXISTS halo.reports(
        device_id UInt64,
        timestamp UInt64, -- timestamp, milliseconds from January 1 1970
        datetime DateTime MATERIALIZED toDateTime(timestamp), -- auto generate date time from ts column
        drop_rate UInt32,
        latency UInt32,
        throughput UInt32
    ) ENGINE = MergeTree()
    ORDER BY (device_id, timestamp);
    CREATE TABLE IF NOT EXISTS halo.predictions(
        device_id UInt64,
        date Date,
        day_of_week UInt8 MATERIALIZED toDayOfWeek(date),
        drop_rate Tuple(Float32, Float32, Float32),
        latency Tuple(Float32, Float32, Float32),
        throughput Tuple(Float32, Float32, Float32)
    ) ENGINE = ReplacingMergeTree()
    ORDER BY (device_id, date);
EOSQL
